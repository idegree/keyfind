from django.shortcuts import render
from game.models import Game


def home(request):
    return render(request, 'keyfind/home.html')


def game(request, slug):
    item = Game.objects.select_related().get(url=slug)
    options = item.extra_options.all()
    modes = item.game_modes.all()
    screens = item.screenshot_set.all()
    similar = item.similar_games.all()
    return render(request, 'keyfind/game.html', {'game': item,
                                                 'options': options,
                                                 'screens': screens,
                                                 'similar': similar,
                                                 'modes': modes})
