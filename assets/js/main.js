(function() {
  $(function() {
    var createCookie, eraseCookie, gcwFtrHeight, gcwLast, readCookie, swiper;
    createCookie = function(name, value, days) {
      var date, expires;
      expires = void 0;
      if (days) {
        date = new Date;
        date.setTime(date.getTime() + days * 24 * 60 * 60 * 1000);
        expires = '; expires=' + date.toGMTString();
      } else {
        expires = '';
      }
      return document.cookie = encodeURIComponent(name) + '=' + encodeURIComponent(value) + expires + '; path=/';
    };
    readCookie = function(name) {
      var c, ca, i, nameEQ;
      nameEQ = encodeURIComponent(name) + '=';
      ca = document.cookie.split(';');
      i = 0;
      while (i < ca.length) {
        c = ca[i];
        while (c.charAt(0) === ' ') {
          c = c.substring(1, c.length);
        }
        if (c.indexOf(nameEQ) === 0) {
          return decodeURIComponent(c.substring(nameEQ.length, c.length));
        }
        i++;
      }
      return null;
    };
    eraseCookie = function(name) {
      return createCookie(name, '', -1);
    };
    if ($('.swiper-container').length) {
      swiper = new Swiper('.swiper-container', {
        pagination: false,
        loop: false,
        freeMode: true,
        centeredSlides: false,
        slidesPerView: 6,
        slidesPerGroup: 6,
        spaceBetween: 8,
        slideClass: 'game-card',
        wrapperClass: 'swiper-wrapper',
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        freeModeMomentum: false,
        breakpoints: {
          1165: {
            slidesPerView: 3,
            slidesPerGroup: 3,
            spaceBetween: 6
          },
          1375: {
            slidesPerView: 4,
            slidesPerGroup: 4,
            spaceBetween: 6
          },
          1585: {
            slidesPerView: 5,
            slidesPerGroup: 5,
            spaceBetween: 7
          }
        }
      });
    }
    if ($('.aside-list-game').length) {
      $('.aside-list-game').height($('#aside').outerHeight() - ($('.hdr-logo-lnk').outerHeight() + $('.nav-aside').outerHeight() + $('.aside-hr').outerHeight() + $('.aside-search').outerHeight()) - 116 + "px");
      $('.aside-list-game').mCustomScrollbar({
        theme: 'minimal-dark'
      });
    }
    if ($('.mwishes-wrap').length) {
      $('.mwishes-wrap').mCustomScrollbar({
        theme: 'minimal-dark'
      });
    }
    if ($('.swiper-container-item').length) {
      swiper = new Swiper('.swiper-container-item', {
        pagination: true,
        pagination: '.swiper-pagination',
        paginationType: 'custom',
        paginationCustomRender: function(swiper, current, total) {
          var title;
          title = $(swiper.slides[swiper.activeIndex]).children().attr("alt");
          return title + '  ' + current + ' / ' + total;
        },
        loop: false,
        slideClass: 'iswlg-item',
        wrapperClass: 'swiper-wrapper',
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        freeModeMomentum: false
      });
    }
    if ($('.gcw--ftr').length) {
      if (($('.game-cards-wrap').length)) {
        gcwLast = $('.game-cards-wrap').eq(-2);
        gcwFtrHeight = $('.gcw--ftr').height() + 13;
        gcwLast.css("marginBottom", gcwFtrHeight);
      }
    }
    if ($('.just-bought').length) {
      if (readCookie("notify")) {
        $('.just-bought').remove();
      } else {
        $.notify.addStyle('findkey', {
          html: "<div class='notify-block-wrap'" + "<div>" + "<div class='time' data-notify-html='time'/>" + "<span data-notify-html='img'> </span>" + "<div class='desc'>" + "<span class='name' data-notify-html='name'></span>" + "<span class='price-title' >забрали за </span>" + "<span class='price' data-notify-text='price'></span>" + "<span class='price-sub'><sup>&nbsp;руб</sup></span>" + "</div>" + "</div>" + "</div>"
        });
        $(".jb-close").click(function(event) {
          $('.just-bought').remove();
          return createCookie("notify", 1, 1);
        });
      }
    }
    if ($('.flexdatalist').length) {
      $('.flexdatalist').flexdatalist({
        minLength: 1,
        noResultsText: "По запросу <b>{keyword}</b> ничего не найдено. Найдите вашу игру в <a href='#'>базе</a>"
      });
      if ($(".jq-ry-container").length) {
        $(".jq-ry-container").rateYo({
          starWidth: "14px",
          spacing: "2px",
          readOnly: true,
          fullStar: true,
          ratedFill: "#ff9c00",
          normalFill: "#e9e9e9"
        });
        $(".jq-ry-container").each(function(indx, element) {
          var rateYoRating;
          rateYoRating = $(element).data('rating');
          return $(element).rateYo("option", "rating", rateYoRating);
        });
      }
      if ($(".tabs").length) {
        $('.tabs').tabslet();
      }
    }
    if ($('.popup-modal').length) {
      $('.popup-modal').magnificPopup({
        type: 'inline',
        preloader: false,
        showCloseBtn: false,
        closeOnBgClick: true,
        closeOnContentClick: false
      });
      $(document).on('click', '.popup-modal-dismiss', function(e) {
        e.preventDefault();
        return $.magnificPopup.close();
      });
    }
    if (($('.wnw-no-authorized').length)) {
      $('.wnw-no-authorized').hover((function() {
        return $(this).addClass('hover');
      }));
      return $(document).click(function(event) {
        if (!$(event.target).closest('.hnw-info, .wnw-no-authorized').length) {
          if ($('.wnw-no-authorized').hasClass('hover')) {
            return $('.wnw-no-authorized.hover').removeClass('hover');
          }
        }
      });
    }
  });

}).call(this);
