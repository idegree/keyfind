from django.contrib import admin
from imagekit.admin import AdminThumbnail
from . import models


@admin.register(models.Activation)
class ActivationAdmin(admin.ModelAdmin):
    list_display = ('id', 'name')
    search_fields = ['name']
    list_per_page = 20


@admin.register(models.Subtitile)
class SubtitileAdmin(admin.ModelAdmin):
    list_display = ('id', 'name')
    search_fields = ['name']
    list_per_page = 20


@admin.register(models.Platform)
class PlatformAdmin(admin.ModelAdmin):
    list_display = ('id', 'name')
    search_fields = ['name']
    list_per_page = 20


@admin.register(models.GameMode)
class GameModeAdmin(admin.ModelAdmin):
    list_display = ('id', 'name')
    search_fields = ['name']
    list_per_page = 20


@admin.register(models.ExtraOption)
class ExtraOptionAdmin(admin.ModelAdmin):
    list_display = ('id', 'name')
    search_fields = ['name']
    list_per_page = 20


class ScreenshotAdmin(admin.TabularInline):
    model = models.Screenshot
admin.register(ScreenshotAdmin)


@admin.register(models.Game)
class GameAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'srp', 'thumbnail')
    thumbnail = AdminThumbnail(image_field='image_196')
    search_fields = ['name', 'description']
    list_per_page = 40
    prepopulated_fields = {'url': ('name',)}
    filter_horizontal = ('platforms', 'extra_options', 'game_modes',
                         'similar_games')
    radio_fields = {'subtitle': admin.VERTICAL, 'activation': admin.VERTICAL}
    inlines = [ScreenshotAdmin]
