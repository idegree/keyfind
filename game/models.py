from django.db import models
from django.urls import reverse
from django.utils import timezone
from imagekit.models import ProcessedImageField, ImageSpecField
from imagekit.processors import ResizeToFit


class Activation(models.Model):
    name = models.CharField(max_length=255, unique=True, verbose_name='Имя')

    def __unicode__(self):
        return str(self.name)

    def __str__(self):
        return str(self.name)

    class Meta:
        verbose_name = 'активацию'
        verbose_name_plural = 'Активации'


class Subtitile(models.Model):
    name = models.CharField(max_length=255, unique=True, verbose_name='Имя')

    def __unicode__(self):
        return str(self.name)

    def __str__(self):
        return str(self.name)

    class Meta:
        verbose_name = 'подстрочник'
        verbose_name_plural = 'Подстрочники'


class Platform(models.Model):
    name = models.CharField(max_length=255, unique=True, verbose_name='Имя')

    def __unicode__(self):
        return str(self.name)

    def __str__(self):
        return str(self.name)

    class Meta:
        verbose_name = 'платформу'
        verbose_name_plural = 'Платформы'


class ExtraOption(models.Model):
    name = models.CharField(max_length=255, unique=True, verbose_name='Имя')

    def __unicode__(self):
        return str(self.name)

    def __str__(self):
        return str(self.name)

    class Meta:
        verbose_name = 'опцию'
        verbose_name_plural = 'Экстра опции'


class GameMode(models.Model):
    name = models.CharField(max_length=255, unique=True, verbose_name='Имя')

    def __unicode__(self):
        return str(self.name)

    def __str__(self):
        return str(self.name)

    class Meta:
        verbose_name = 'режим игры'
        verbose_name_plural = 'Режимы игры'


class Game(models.Model):
    name = models.CharField(max_length=255, unique=True, verbose_name='Имя')
    subtitle = models.ForeignKey('Subtitile', verbose_name='Подстрочник')
    url = models.SlugField(max_length=255, unique=True, db_index=True,
                           verbose_name='ЧПУ')
    description = models.TextField(verbose_name='Описание')
    release_date = models.DateField(default=timezone.now,
                                    verbose_name='Дата выхода')
    srp = models.DecimalField(max_digits=12, decimal_places=2,
                              verbose_name='РРЦ',
                              help_text='Рекомендованная розничная цена')
    youtube = models.URLField(verbose_name='Youtube', blank=True, null=True,
                              help_text='Ссылка на видео')
    activation = models.ForeignKey('Activation', verbose_name='Активация')
    image = ProcessedImageField(upload_to='images', verbose_name='Обложка')
    image_196 = ImageSpecField(source='image', processors=[ResizeToFit(196)],
                               format='JPEG', options={'quality': 99,
                                                       'optimize': True,
                                                       'progressive': True})
    image_344 = ImageSpecField(source='image', processors=[ResizeToFit(344)],
                               format='JPEG', options={'quality': 99,
                                                       'optimize': True,
                                                       'progressive': True})
    platforms = models.ManyToManyField('Platform', blank=True, null=True,
                                       verbose_name='Платформы')
    extra_options = models.ManyToManyField('ExtraOption', blank=True, null=True,
                                           verbose_name='Экстра опции')
    game_modes = models.ManyToManyField('GameMode', blank=True, null=True,
                                        verbose_name='Режимы игры')
    similar_games = models.ManyToManyField('self', blank=True, null=True,
                                           verbose_name='Похожие игры')

    def __unicode__(self):
        return str(self.name)

    def __str__(self):
        return str(self.name)

    def get_absolute_url(self):
        return reverse('game', kwargs={'slug': self.url})

    class Meta:
        verbose_name = 'игру'
        verbose_name_plural = 'Игры'


class Screenshot(models.Model):
    alt = models.CharField(max_length=255, verbose_name='Подпись к картинке')
    image = ProcessedImageField(processors=[ResizeToFit(height=700)],
                                format='JPEG', options={'quality': 99,
                                                        'optimize': True,
                                                        'progressive': True})
    game = models.ForeignKey('Game', verbose_name='Игра')
