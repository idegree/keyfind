from . import models


def base(request):
    games = models.Game.objects.values_list('name', 'url')
    games_amount = games.count()
    return {'games': games, 'games_amount': games_amount}
